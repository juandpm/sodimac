const express = require('express')
const apiRoutes = require("./routes")
const bodyParser = require('body-parser');
const cors = require('cors')
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors())

var port = process.env.PORT || 8080;

app.get('/', cors(), (req, res) => res.send('Welcome to Express'));

app.listen(port, function() {
    console.log("Running FirstRest on Port "+ port);
})

app.use('/api', apiRoutes)