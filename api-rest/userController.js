//For index

let users = [{name:"Juan",age:28}]

exports.index = function (req, res) {
   
    res.json({
        status: "success",
        message: "Got Users Successfully!",
        data: users       
    });
    
};

//For creating new bio
exports.add = function (req, res) {
    var user = {name:"",age:0};
    user.name = req.body.name;
    user.age = req.body.age;

    users.push(user)
        res.json({
            message: "New Bio Added!",
            data: users
        });
};