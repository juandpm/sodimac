//routes.js
//initialize express router
let router = require('express').Router();
//set default API response
router.get('/', function(req, res) {
    res.json({
        status: 'API Works',
        message: 'Welcome to FirstRest API'
    });
});

//Import Bio Controller
var userController = require('./userController');
// Bio routes
router.route('/user')
    .get(userController.index)
    .post(userController.add);

//Export API routes
module.exports = router;